const applyStyles = (element, event = 'enter') => {
    const checkbox = document.getElementById(element.querySelector('.selectItem').getAttribute('for'));
    if (checkbox && !checkbox.hasAttribute('disabled')) {
        let state = '';
        if (checkbox.checked) {
            state = 'selectedHover';
        } else {
            state = 'hover';
        }

        const subHeading = element.querySelector('.subHeading');

        if (event === 'leave') {
            if (checkbox.checked) {
                subHeading.innerText = 'Котэ не одобряет?';
                subHeading.classList.add('text-selected');
            }

            element.classList.add(`fedTheCat__tile-${state}`)
        } else {
            if (subHeading.classList.contains('text-selected')) {
                subHeading.classList.remove('text-selected');
                subHeading.innerText = 'Сказочное заморское яство';
            }

            element.classList.remove(`fedTheCat__tile-${state}`)
        }
    }
}

const catTiles = document.querySelectorAll('.fedTheCat__tile');
const catTilesLen = catTiles.length;

for (let i = 0; i < catTilesLen; i += 1) {
    const currentTile = catTiles[i];

    currentTile.addEventListener('mouseenter', () => {
        applyStyles(currentTile);
    });

    currentTile.addEventListener('mouseleave', () => {
        applyStyles(currentTile, 'leave');
    });
}

